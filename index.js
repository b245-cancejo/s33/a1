//console.log("hellow");

fetch("https://jsonplaceholder.typicode.com/todos",{method: "GET"})
.then(response => response.json())
.then(todos => console.log(todos));



fetch("https://jsonplaceholder.typicode.com/todos",{method: "GET"})
.then(response => response.json())
.then(todos => {
	let title = todos.map(todo => todo.title)
	console.log(title)
});



fetch("https://jsonplaceholder.typicode.com/todos/5",{method: "GET"})
.then(response => response.json())
.then(todos => {
	console.log(todos)
	console.log(`The item "${todos.title}" on the list has a status of ${todos.completed}`)
});


//post method
fetch("https://jsonplaceholder.typicode.com/todos",{method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item"
	})
})
.then(res => res.json())
.then(todos => console.log(todos));


//8. put method
fetch("https://jsonplaceholder.typicode.com/todos/1",{method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item"
	})
})
.then(res => res.json())
.then(todos => console.log(todos));


//9
fetch("https://jsonplaceholder.typicode.com/todos/1",{method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted:"Pending",
		description: "To update the my to do list with a different data structure",
		Status: "Pending",
		userId: 1,
		title: "Updated To Do List Item"
	})
})
.then(res => res.json())
.then(todos => console.log(todos));


//10
fetch("https://jsonplaceholder.typicode.com/todos/1",{method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated to do list Item"
		
	})
})
.then(res => res.json())
.then(todos => console.log(todos));



//11

fetch("https://jsonplaceholder.typicode.com/todos/1",{method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted:"01/31/2023",
		Status: "Completed",
		userId: 1
	})
})
.then(res => res.json())
.then(todos => console.log(todos));

//12

fetch("https://jsonplaceholder.typicode.com/todos/2", {method: "DELETE"})
.then(response => response.json())
.then(result => console.log(result));


